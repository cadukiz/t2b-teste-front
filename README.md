# Trend2B - Teste Front
Fazer tela de login e listagem de empresas feito em Angular ou Vue.

Neste teste iremos avaliar sua estrutura de pastas, divisão de componentes e código.


# Pré-requisitos:
  - Bootstrap (Layout a seu critério)
  - Criar um componente de listagem utilizando Datatable(ou equivalente). (Com busca geral, ordenação, paginação)
  - Icone Premium também deve ser um componente.
  - Criar repositório com o teste realizado e instruções de instalação.

# Funcionalidades
  - Login: Autenticação
  - Listagem de empresas :
    - Colunas:
        - Id -> id,
        - Premium: -> premium adicionar Icone: ![Star Icon](https://material.io/tools/icons/static/icons/baseline-star-24px.svg) true = Amarerelo e false = Cinza claro
        - Empresa  ->  name
        - Fantasia -> alias
        - Grupo -> group.name
        - Endereço -> address -  icone Spot - tooltip com todos campos address concatenados
        - Documento: -> document (formatar CNPJ 00.000.000/0000-00)
        - Ultima Atualização: -> updated_a formato (DD/MM/AAAA HH:MM:SS)
        - Status -> status (Opções Ativo e Inativo)
    - Filtros: 
        - Grupo 
        - Premium
        - Status

### Referências:
* [Datatable](https://coderthemes.com/ubold/layouts/light/tables-datatables.html) - Datatable responsiva, ordenação e filtro
* [Material Icons](https://material.io/tools/icons/?icon=star&style=baseline) - Icones
* [Angular](https://angular.io/)
* [Vue](https://vuejs.org/)

### Endpoints

Login:
http://devapi.simt2b.com.br:803/api/login (POST)

Empresas:
http://devapi.simt2b.com.br:803/api/companies?embed=group (GET)

* para filtra basta adicionar o nome do campo e valor (campo=valor) exemplo:
http://devapi.simt2b.com.br:803/api/companies?embed=group&status=1&grupo_id=10  (GET)